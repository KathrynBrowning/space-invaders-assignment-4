﻿using Windows.UI.Xaml.Controls;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates a bullet sprite.
    /// </summary>
    public sealed partial class BulletSprite : ISpriteRenderer
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BulletSprite" /> class.
        ///     Precondition: None
        ///     Postcondition: Bullet sprite has been initialized
        /// </summary>
        public BulletSprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: Sprite has been rendered at the specified coordinates.s
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }
    }
}
