﻿using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates the level 3 enemy ships
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    public sealed partial class EnemyShipLevel3Sprite : ISpriteRenderer
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel3Sprite"/> class.
        ///     Precondition: None
        ///     Postcondition: The level three enemy ships have been initialized.
        /// </summary>
        public EnemyShipLevel3Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: The sprite has been rendered at the specified coordinates.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }

        /// <summary>
        ///     Changes the color of the sprite's laser.
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed
        /// </summary>
        public void ChangeLaserColor()
        {
            var brush = this.laser.Fill as SolidColorBrush;
            if (brush == null)
            {
                return;
            }

            if (brush.Color == Colors.DarkGoldenrod)
            {
                this.laser.Fill = new SolidColorBrush(Colors.DeepSkyBlue);
            }
            else
            {
                this.laser.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
            }
        }
    }
}
