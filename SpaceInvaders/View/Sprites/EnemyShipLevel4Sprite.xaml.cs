﻿using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates the enemy ship level 4 sprite
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    public sealed partial class EnemyShipLevel4Sprite : ISpriteRenderer
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel4Sprite"/> class.
        ///      Precondition: None
        ///     Postcondition: Level two enemy ships have been initialized.
        /// </summary>
        public EnemyShipLevel4Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: The sprite has been rendered at the specified coordinates.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }

        /// <summary>
        ///     Changes the color of the sprite's weapon and part of its chasis.
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed.
        /// </summary>
        public void ChangeSpriteColor()
        {
            var brush = this.weapon.Fill as SolidColorBrush;
            if (brush == null)
            {
                return;
            }

            if (brush.Color == Colors.Black)
            {
                this.weapon.Fill = new SolidColorBrush(Colors.DeepSkyBlue);
                this.chasis1.Fill = new SolidColorBrush(Colors.DimGray);
                this.chasis2.Fill = new SolidColorBrush(Colors.DimGray);
                this.chasis3.Fill = new SolidColorBrush(Colors.DimGray);
                this.chasis4.Fill = new SolidColorBrush(Colors.DimGray);
                this.chasis5.Fill = new SolidColorBrush(Colors.DimGray);
            }
            else
            {
                this.weapon.Fill = new SolidColorBrush(Colors.Black);
                this.chasis1.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
                this.chasis2.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
                this.chasis3.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
                this.chasis4.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
                this.chasis5.Fill = new SolidColorBrush(Colors.DarkGoldenrod);
            }
        }
    }
}
