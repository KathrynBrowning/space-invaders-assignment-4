﻿using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates an EnemyShipLevel1Sprite that inherits rendering capabilities.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class EnemyShipLevel1Sprite : ISpriteRenderer
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel1Sprite"/> class.
        ///     Precondition: None
        ///     Postcondition: Class is initialized.s
        /// </summary>
        public EnemyShipLevel1Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: Sprite has been rendered at specified coordinates
        /// </summary>
        /// <param name="x">The x location</param>
        /// <param name="y">The y location</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }
    }
}
