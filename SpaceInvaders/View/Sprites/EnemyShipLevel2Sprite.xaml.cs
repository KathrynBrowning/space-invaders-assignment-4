﻿using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace SpaceInvaders.View.Sprites
{
    /// <summary>
    ///     Creates the enemy ship level two sprite.
    /// </summary>
    /// <seealso cref="Windows.UI.Xaml.Controls.UserControl" />
    /// <seealso cref="SpaceInvaders.View.Sprites.ISpriteRenderer" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector" />
    /// <seealso cref="Windows.UI.Xaml.Markup.IComponentConnector2" />
    public sealed partial class EnemyShipLevel2Sprite : ISpriteRenderer
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShipLevel2Sprite"/> class.
        ///     Precondition: None
        ///     Postcondition: Level two enemy ships have been initialized.
        /// </summary>
        public EnemyShipLevel2Sprite()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        ///     Precondition: None
        ///     Postcondition: The sprite has been rendered at the specified coordinates.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this, x);
            Canvas.SetTop(this, y);
        }

        /// <summary>
        ///     Changes the color of the sprite's eyes.
        ///     Precondition: SolidColorBrush != null
        ///     Postcondition: Sprite's color has been changed
        /// </summary>
        public void ChangeEyeColor()
        {
            var brush = this.leftEye.Fill as SolidColorBrush;
            if (brush == null)
            {
                return;
            }

            if (brush.Color == Colors.Black)
            {
                this.leftEye.Fill = new SolidColorBrush(Colors.Red);
                this.rightEye.Fill = new SolidColorBrush(Colors.Red);
            }
            else
            {
                this.leftEye.Fill = new SolidColorBrush(Colors.Black);
                this.rightEye.Fill = new SolidColorBrush(Colors.Black);
            }
        }
    }
}
