﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level two enemy ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    internal class EnemyShipLevel2 : EnemyShip
    {
        public EnemyShipLevel2() : base(2)
        {
            Sprite = new EnemyShipLevel2Sprite();
        }

        public override void Animate()
        {
            ((EnemyShipLevel2Sprite)Sprite).ChangeEyeColor();
        }
    }
}
