﻿using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the player ship.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public class PlayerShip : GameObject
    {
        #region Data members

        private const int SpeedXDirection = 3;
        private const int SpeedYDirection = 0;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlayerShip" /> class.
        /// </summary>
        public PlayerShip()
        {
            Sprite = new PlayerShipSprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
        }

        /// <summary>
        ///     Checks for a collision between an enemy's bullet and the player ship
        /// <param name="background">The background.</param>
        /// <param name="bulletPosition">The bullet position.</param>
        /// <returns>True if there has been a collision, false otherwise</returns>
        /// </summary>
        public bool CheckForCollision(Canvas background, Point bulletPosition)
        {
            Sprite.Opacity = 100;

            if (Sprite.Opacity <= 0)
            {
                return false;
            }
            var spriteLocation = Sprite.TransformToVisual(background).TransformPoint(new Point(0, 0));

            var left = spriteLocation.X;
            var right = left + Sprite.Width;
            var top = spriteLocation.Y;
            var bottom = top + Sprite.Height;

            if (!(bulletPosition.X > left) || !(bulletPosition.X < right) || !(bulletPosition.Y > top) ||
                !(bulletPosition.Y < bottom))
            {
                return false;
            }
            Sprite.Opacity = 0;         
            return true;
        }

        #endregion
    }
}