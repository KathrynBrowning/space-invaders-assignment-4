﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    internal class PlayerLife : GameObject
    {
        public PlayerLife()
        {
            Sprite = new PlayerLifeSprite();
        }
    }
}
