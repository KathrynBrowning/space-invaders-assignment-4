﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level 4 enemy ship
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    internal class EnemyShipLevel4 : EnemyShip
    {
        public EnemyShipLevel4() : base(4)
        {
            Sprite = new EnemyShipLevel4Sprite();
        }

        public override void Animate()
        {
            ((EnemyShipLevel4Sprite)Sprite).ChangeSpriteColor();
        }
    }
}
