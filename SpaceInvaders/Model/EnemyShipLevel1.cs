﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates the level one enemy ships
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    internal class EnemyShipLevel1 : EnemyShip
    {
        public EnemyShipLevel1() : base(1)
        {
            Sprite = new EnemyShipLevel1Sprite();
        }

        public override void Animate()
        {
            //throw new System.NotImplementedException();
        }
    }
}
