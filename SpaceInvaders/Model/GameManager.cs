﻿using System;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Manages the entire game.
    /// </summary>
    public class GameManager
    {
        #region Data members

        /// <summary>
        ///     The frequency of the dispatcher timer ticks, in milliseconds.
        /// </summary>
        public const int TickInterval = 500;

        private const int EnemyShipSpeed = 5;
        private const int NumberOfLvl1Ships = 2;
        private const int NumberOfLvl2Ships = 4;
        private const int NumberOfLvl3Ships = 6;
        private const int NumberOfLvl4Ships = 8;
        private const int TotalPoints = 24;
        private const int ScoreY = 300;
        private const int ScoreX = 50;
        private const double PlayerShipBottomOffset = 5;

        private readonly TimeSpan gameTickInterval = new TimeSpan(0, 0, 0, 0, TickInterval);
        private readonly double backgroundHeight;
        private readonly double backgroundWidth;
        private readonly List<EnemyShip> enemyFleet;

        private DispatcherTimer timer;
        private Grid shipsGrid;
        private PlayerShip playerShip;
        private Bullet playerBullet;
        private Bullet enemyBullet;
        private Canvas background;
        private TextBlock scoreText;

        private int shipPoints;
        private int enemyShipTickCount = 20;
        private int enemyShipMovement = EnemyShipSpeed;
        private int count;

        private List<PlayerLife> playerLives;
        private PlayerLife playerLife1;
        private PlayerLife playerLife2;
        private PlayerLife playerLife3;



        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="GameManager" /> class.
        ///     Precondition: backgroundHeight > 0 AND backgroundWidth > 0
        /// </summary>
        /// <param name="backgroundHeight">The backgroundHeight of the game play window.</param>
        /// <param name="backgroundWidth">The backgroundWidth of the game play window.</param>
        public GameManager(double backgroundHeight, double backgroundWidth)
        {
            if (backgroundHeight <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundHeight));
            }

            if (backgroundWidth <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(backgroundWidth));
            }

            this.backgroundHeight = backgroundHeight;
            this.backgroundWidth = backgroundWidth;

            this.shipPoints = 0;
            this.enemyFleet = new List<EnemyShip>();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Initializes the game placing player ship and enemy ship in the game.
        ///     Precondition: background != null
        ///     Postcondition: Game is initialized and ready for play.
        /// </summary>
        /// <param name="theBackground">The background canvas.</param>
        /// <param name="theShipsGrid">The ships grid.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public void InitializeGame(Canvas theBackground, Grid theShipsGrid)
        {
            if (theBackground == null)
            {
                throw new ArgumentNullException(nameof(theBackground));
            }

            this.shipsGrid = theShipsGrid;
            this.background = theBackground;

            this.createAndPlacePlayerShip();
            this.createAndPlaceEnemyShips();
            this.initializeTimer();
        }

        /// <summary>
        ///     Renders the sprite at the specified location.
        /// </summary>
        /// <param name="x">The x location</param>
        /// <param name="y">The y location</param>
        public void RenderAt(double x, double y)
        {
            Canvas.SetLeft(this.shipsGrid, x);
            Canvas.SetTop(this.shipsGrid, y);
        }

        /// <summary>
        ///     Creates and places the player bullet in front and center of the player ship.
        ///     Precondition: playerBullet != null
        ///     Postcondition: playerBullet has been created and has been placed on the canvas.
        /// </summary>
        public void CreateAndPlacePlayerBullet()
        {
            if (this.playerBullet != null)
            {
                return;
            }

            this.playerBullet = new Bullet();
            this.background.Children.Add(this.playerBullet.Sprite);

            this.playerBullet.X = this.playerShip.Width/2 + this.playerShip.X;
            this.playerBullet.Y = this.playerShip.Y;
        }

        /// <summary>
        ///     Moves the enemy ships left and right on a timer.
        ///     Precondition: none
        ///     Postcondition: The player ship has been moved left or right with each tick
        /// </summary>
        public void MoveShips()
        {
            var left = Canvas.GetLeft(this.shipsGrid);
            const int availableLeftSpace = 7;
            const int availableRightSpace = 7;

            this.enemyShipTickCount++;
            if (this.enemyShipMovement > 0 && this.enemyShipTickCount > availableRightSpace)
            {
                this.enemyShipTickCount = 0;
                this.enemyShipMovement = -this.enemyShipMovement;
            }
            else if (this.enemyShipMovement < 0 && this.enemyShipTickCount > availableLeftSpace)
            {
                this.enemyShipTickCount = 0;
                this.enemyShipMovement = -this.enemyShipMovement;
            }
            Canvas.SetLeft(this.shipsGrid, left + this.enemyShipMovement);
        }

        /// <summary>
        ///     Moves the player ship to the left.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved left.
        /// </summary>
        public void MovePlayerShipLeft()
        {
            var applicationBeginningWidth = 0;
            if (this.playerShip.X <= applicationBeginningWidth)
            {
                this.playerShip.MoveRight();
            }
            this.playerShip.MoveLeft();
        }

        /// <summary>
        ///     Moves the player ship to the right.
        ///     Precondition: none
        ///     Postcondition: The player ship has moved right.
        /// </summary>
        public void MovePlayerShipRight()
        {
            if (this.playerShip.X >= (this.backgroundWidth - this.playerShip.Width))
            {
                this.playerShip.MoveLeft();
            }
            this.playerShip.MoveRight();
        }

        private void movePlayerBulletUp()
        {
            this.playerBullet?.MoveUp();
            this.bulletBoundaryCollisionDetection();
        }

        private void moveEnemyBulletsDown()
        {
            this.createAndPlaceEnemyBullets();
            this.enemyBullet?.MoveDown();
            this.removeEnemyBullet();
        }

        private void removeEnemyBullet()
        {
            if (!(this.enemyBullet?.Y > this.background.Height))
            {
                return;
            }
            this.background.Children.Remove(this.enemyBullet.Sprite);
            this.enemyBullet = null;
        }

        private void createAndPlaceEnemyBullets()
        {
            if (this.enemyBullet != null)
            {
                return;
            }

            this.enemyBullet = new Bullet();
            this.randomEnemyBulletFiring();
        }

        private void randomEnemyBulletFiring()
        {
            var firingEnemyShips =
                this.enemyFleet.FindAll(
                    s => (s.GetType() == typeof(EnemyShipLevel3) || s.GetType() == typeof(EnemyShipLevel4)) && s.IsAlive);
            var generator = new Random();
            var rand = generator.Next(firingEnemyShips.Count);
            var ship = firingEnemyShips[rand];

            var spriteLocation = ship.Sprite.TransformToVisual(this.background).TransformPoint(new Point(0, 0));
            var center = spriteLocation.X + ship.Width/2;
            var bottom = spriteLocation.Y + ship.Sprite.Height;

            this.enemyBullet.X = center;
            this.enemyBullet.Y = bottom;
            this.background.Children.Add(this.enemyBullet.Sprite);
        }

        private void createAndPlacePlayerShip()
        {
            this.playerShip = new PlayerShip();
            this.background.Children.Add(this.playerShip.Sprite);

            this.placePlayerShipNearBottomOfBackgroundCentered();

        }

        private void placePlayerShipNearBottomOfBackgroundCentered()
        {
            this.playerShip.X = this.backgroundWidth/2 - this.playerShip.Width/2.0;
            this.playerShip.Y = this.backgroundHeight - this.playerShip.Height - PlayerShipBottomOffset;
        }

        private void createAndPlaceEnemyShips()
        {
            this.createColumns();
            this.placeLevelOneEnemyShips();
            this.placeLevelTwoEnemyShips();
            this.placeLevelThreeEnemyShips();
            this.placeLevelFourEnemyShips();
        }

        private void createColumns()
        {
            for (var i = 0; i < NumberOfLvl4Ships; i++)
            {
                this.shipsGrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
        }

        private void placeLevelOneEnemyShips()
        {
            for (var i = 0; i < NumberOfLvl1Ships; i++)
            {
                var enemyShipLevelOne = new EnemyShipLevel1();
                this.enemyFleet.Add(enemyShipLevelOne);

                enemyShipLevelOne.Sprite.SetValue(Grid.RowProperty, 3);
                enemyShipLevelOne.Sprite.SetValue(Grid.ColumnProperty, i + 3);
                enemyShipLevelOne.Sprite.Margin = new Thickness(3, 0, 3, 0);
                this.shipsGrid.Children.Add(enemyShipLevelOne.Sprite);
            }
        }

        private void placeLevelTwoEnemyShips()
        {
            for (var i = 0; i < NumberOfLvl2Ships; i++)
            {
                var enemyShipLevelTwo = new EnemyShipLevel2();
                this.enemyFleet.Add(enemyShipLevelTwo);

                enemyShipLevelTwo.Sprite.SetValue(Grid.RowProperty, 2);
                enemyShipLevelTwo.Sprite.SetValue(Grid.ColumnProperty, i + 2);
                enemyShipLevelTwo.Sprite.Margin = new Thickness(3, 0, 3, 0);
                this.shipsGrid.Children.Add(enemyShipLevelTwo.Sprite);
            }
        }

        private void placeLevelThreeEnemyShips()
        {
            for (var i = 0; i < NumberOfLvl3Ships; i++)
            {
                var enemyShipLevelThree = new EnemyShipLevel3();
                this.enemyFleet.Add(enemyShipLevelThree);

                enemyShipLevelThree.Sprite.SetValue(Grid.RowProperty, 1);
                enemyShipLevelThree.Sprite.SetValue(Grid.ColumnProperty, i + 1);
                enemyShipLevelThree.Sprite.Margin = new Thickness(3, 0, 3, 0);
                this.shipsGrid.Children.Add(enemyShipLevelThree.Sprite);
            }
        }

        private void placeLevelFourEnemyShips()
        {
            for (var i = 0; i < NumberOfLvl4Ships; i++)
            {
                var enemyShipLevelFour = new EnemyShipLevel4();
                this.enemyFleet.Add(enemyShipLevelFour);

                enemyShipLevelFour.Sprite.SetValue(Grid.RowProperty, 0);
                enemyShipLevelFour.Sprite.SetValue(Grid.ColumnProperty, i);
                enemyShipLevelFour.Sprite.Margin = new Thickness(3, 0, 3, 0);
                this.shipsGrid.Children.Add(enemyShipLevelFour.Sprite);
            }
        }

        private void animateEnemyShips()
        {
            this.count ++;
            if (this.count <= 5)
            {
                return;
            }
            foreach (var enemyShip in this.enemyFleet)
            {
                enemyShip.Animate();
            }
            this.count = 0;
        }

        private void enemyShipCollisionDetection()
        {
            foreach (var enemyShip in this.enemyFleet)
            {
                if (this.playerBullet == null ||
                    !enemyShip.CheckForCollision(this.background, new Point(this.playerBullet.X, this.playerBullet.Y)))
                {
                    continue;
                }
                this.shipPoints += enemyShip.GetShipPoints();
                this.removePlayerBullet();

                this.removeScore();
                this.updateScore();
            }
        }

        private void playerShipCollisionDetection()
        {
            //TODO
            for (var i = 0; i < this.playerLives.Count; i++)
            {
                if (this.enemyBullet == null ||
                    !this.playerShip.CheckForCollision(this.background,
                        new Point(this.enemyBullet.X, this.enemyBullet.Y)))
                {
                    continue;
                }

                this.removeEnemyBullet();
                this.removePlayerBullet();
                this.removePlayerLifeAndEndGame();
            }
        }

        private void removePlayerLifeAndEndGame()
        {
            this.playerLives[0].Sprite.Opacity = 0;
            this.background.Children.Remove(this.playerLives[0].Sprite);
            this.playerLives.Remove(this.playerLives[0]);

            if (this.playerLives.Count == 0)
            {
                this.gameOver();
            }
        }

        private void timerOnTick(object sender, object o)
        {
            this.MoveShips();
            this.createAndPlacePlayerLives();
            this.movePlayerBulletUp();
            this.animateEnemyShips();
            this.moveEnemyBulletsDown();
            this.enemyShipCollisionDetection();
            this.playerShipCollisionDetection();
            this.killedAllEnemies();
        }

        private void bulletBoundaryCollisionDetection()
        {
            if (this.playerBullet != null && this.playerBullet.Y < 0)
            {
                this.removePlayerBullet();
            }
        }

        private void removePlayerBullet()
        {
            if (this.playerBullet == null)
            {
                return;
            }
            this.background.Children.Remove(this.playerBullet.Sprite);
            this.playerBullet = null;
        }

        private void killedAllEnemies()
        {
            if (this.shipPoints != TotalPoints)
            {
                return;
            }
            var playerWon = new TextBlock {
                Text = "YOU WON",
                Foreground = new SolidColorBrush(Colors.AntiqueWhite)
            };
            this.background.Children.Add(playerWon);
            Canvas.SetTop(playerWon, ScoreY);
            Canvas.SetLeft(playerWon, ScoreX);
        }

        private void gameOver()
        {
            this.timer.Stop();
            var playerLost = new TextBlock
            {
                Text = "GAME OVER",
                Foreground = new SolidColorBrush(Colors.AntiqueWhite)
            };
            this.background.Children.Add(playerLost);
            Canvas.SetTop(playerLost, 350);
            Canvas.SetLeft(playerLost, 70);

        }

        private void updateScore()
        {
            this.scoreText = new TextBlock {
                Text = "Score: " + this.shipPoints,
                Foreground = new SolidColorBrush(Colors.AntiqueWhite)
            };
            this.background.Children.Add(this.scoreText);
            Canvas.SetTop(this.scoreText, 300);
            Canvas.SetLeft(this.scoreText, 30);
        }

        private void removeScore()
        {
            this.background.Children.Remove(this.scoreText);
        }

        private void createAndPlacePlayerLives()
        {
            this.playerLife1 = new PlayerLife();
            this.playerLife2 = new PlayerLife();
            this.playerLife3 = new PlayerLife();

            this.playerLives = new List<PlayerLife> {this.playerLife1, this.playerLife2, this.playerLife3};

            foreach (var playerLife in this.playerLives)
            {
                this.background.Children.Add(playerLife.Sprite);
                Canvas.SetTop(playerLife.Sprite, 460);
            }

            Canvas.SetLeft(this.playerLives[0].Sprite, 20);
            Canvas.SetLeft(this.playerLives[1].Sprite, 40);
            Canvas.SetLeft(this.playerLives[2].Sprite, 60);
        }

        private void initializeTimer()
        {
            if (this.timer == null)
            {
                this.timer = new DispatcherTimer();
            }
            this.timer.Tick += this.timerOnTick;
            this.timer.Interval = this.gameTickInterval;
            this.timer.Start();
        }

        #endregion
    }
}