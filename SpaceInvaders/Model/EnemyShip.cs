﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Base EnemyShip class
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    public abstract class EnemyShip : GameObject
    {
        private readonly int shipPoints;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EnemyShip"/> class.
        /// </summary>
        /// <param name="shipPoints">The ship points.</param>
        protected EnemyShip(int shipPoints)
        {
            this.shipPoints = shipPoints;
        }

        /// <summary>
        ///     Checks for a collision between a bullet and enemy ship sprite.
        ///     Precondition: Sprite opacity is less than or equal to 0
        ///     Postcondition: none
        /// </summary>
        /// <param name="background">The background.</param>
        /// <param name="bulletPosition">The bullet position.</param>
        /// <returns>True if a bullet has collided with a ship, false otherwise.</returns>
        public bool CheckForCollision(Canvas background, Point bulletPosition)
        {
            if (Sprite.Opacity <= 0)
            {
                return false;
            }
            var spriteLocation = Sprite.TransformToVisual(background).TransformPoint(new Point(0, 0));

            var left = spriteLocation.X;
            var right = left + Sprite.Width;
            var top = spriteLocation.Y;
            var bottom = top + Sprite.Height;

            if (!(bulletPosition.X > left) || !(bulletPosition.X < right) || !(bulletPosition.Y > top) ||
                !(bulletPosition.Y < bottom))
            {
                return false;
            }
            Sprite.Opacity = 0;
            return true;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is alive.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is alive; otherwise, <c>false</c>.
        /// </value>
        public bool IsAlive => Sprite.Opacity > 0;

        /// <summary>
        ///     Gets the ship points of the specified ship type.
        ///     Precondition: None
        ///     Postcondition: None
        /// </summary>
        /// <returns>The ship points, which differ depending on the ship.</returns>
        public int GetShipPoints()
        {
            return this.shipPoints;
        }

        /// <summary>
        ///     Animates this enemy ships.
        /// </summary>
        public abstract void Animate();
    }
}
