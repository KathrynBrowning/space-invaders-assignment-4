﻿using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a bullet.
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.GameObject" />
    internal class Bullet : GameObject
    {
        private const int SpeedXDirection = 0;
        private const int SpeedYDirection = 50;

        public Bullet()
        {
            Sprite = new BulletSprite();
            SetSpeed(SpeedXDirection, SpeedYDirection);
        }

        public bool CheckForCollision(Canvas background, Point bulletPosition)
        {
            var top = background.Height;

            if (!(bulletPosition.Y >= top))
            {
                return false;
            }
            //Sprite.Opacity = 0;
            return true;
        }

    }
}
