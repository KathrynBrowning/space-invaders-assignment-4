﻿using SpaceInvaders.View.Sprites;

namespace SpaceInvaders.Model
{
    /// <summary>
    ///     Creates a level three enemy ship
    /// </summary>
    /// <seealso cref="SpaceInvaders.Model.EnemyShip" />
    internal class EnemyShipLevel3 : EnemyShip
    {
        public EnemyShipLevel3() : base(3)
        {
            Sprite = new EnemyShipLevel3Sprite();
        }

        public override void Animate()
        {
            ((EnemyShipLevel3Sprite)Sprite).ChangeLaserColor();
        }
    }
}
